#include <stdio.h>

 struct StudentType  //a properly defined struct
{   
    int id_number;   
    char name[50];   
    int age;    
};

int main ()
{ 
    
 struct StudentType  stu1 ={1, "Archibald Henderson", 22};
 struct StudentType stu2={2, "Ophae Mae", 20};   //properly declares a variable similar to struct foo var
 struct StudentType *stuptr;        // declare a pointer to a Structure     

//stu1.age = 22;
//stu1.id_number = 1;
//stu1.name = ”Archibald Henderson”;
//stu2.age = 20;
//stu2.name = "Opha Mae";
stuptr=&stu2;
stuptr-> age=18;  //accesses a variable in a pointer to a structure, *b  = b->var
	
  printf ("Student 1 name:%s\t", stu1.name); //accesses a variable in structure b = b.var
  printf ("Student 2 name:%s\t", stuptr->name);   //accesses a variable in a pointer to a structure, *b  = b->var
  printf ("age:%d\n", stuptr->age); //accesses a variable in a pointer to a structure *b  = b->var
 return(0);
}
