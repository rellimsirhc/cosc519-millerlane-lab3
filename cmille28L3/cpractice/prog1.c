#include <stdio.h>

int main () 
{ 
  /* variable definition: */ 
 int z;      
 int* zp; //pointer to interger*
  
 /* actual initialization */   
 zp=&z;       //assigning address of z to zp
*zp=2735;     //*zp refers to value of z
 
 printf("value of z as decimal integer: %d \n", z);
 printf("value of z as decimal min 6 wide: %6d \n", z); 
  printf("value of octal z : %o \t", z); 
 printf("value of z as hexadecimal is  : %x \n", z);  
 printf("value of z in scientific notation: %e \n", (float)z);
 printf("value of z as float point: %f \n", (float)z); 
 printf("value of z as min 4 wide float point: %4f \n", (float)z);
 printf("value of z a 4 char precion float point: %.4f \n", (float)z); 
 printf("value of z as 3 wide float point: %3.2f \n", (float)z);
 printf("value of z as character: %c \t", z);
 //printf("value of z as string: %s \n", z);
 printf("value of z as unsigned integer : %u \t", z);

 return 0; 
} 
