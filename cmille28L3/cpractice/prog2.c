#include <stdio.h>

int main () 
{ 
  /* variable definition: */ 
  
 int x;	// allocates space in memory for sizeof int, at an address 
 int *xpt;	//declaration of pointer to interger*
 		//alternat pointer to interger*   int* xp; ?
 
  
 /* actual initialization */   
 xpt=&x;       //assigning address of x to xptr
 x=334477;

 printf("Memory address of integer variable x  %p\t", xpt);
 printf("value stored at address pointed to by x = %d \n", *xpt);
  
 return(0); 
} 
